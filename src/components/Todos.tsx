import React, { useContext } from "react";
import { TodosContext } from "../store/todos-context";
import TodoItem from "./TodoItem";

const Todos: React.FC = () => {
  const todosCtx = useContext(TodosContext);
  const deleteHandler = (id: string) => {
    todosCtx.removeTodo(id);
  };

  return (
    <ul className="todos">
      {todosCtx.items.map((item) => (
        <TodoItem
          onDelete={deleteHandler}
          key={item.id}
          id={item.id}
          text={item.text}
        />
      ))}
    </ul>
  );
};

export default Todos;
