import React from "react";

const TodoItem: React.FC<{id:string, text: string; onDelete: (text: string) => void }> = (
  props
) => {
  const deleteHandler = (e: React.MouseEvent) => {
    props.onDelete(props.id);
  };
  return (
    <li onClick={deleteHandler} className="item">
      {props.text}
    </li>
  );
};

export default TodoItem;
