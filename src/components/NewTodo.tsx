import React, { useContext } from "react";
import { useRef } from "react";
import { TodosContext } from "../store/todos-context";

const NewTodo: React.FC= () => {
  const todoTextInput = useRef<HTMLInputElement>(null);

  const todosCtx = useContext(TodosContext);
  
  const submitHandler = (e: React.FormEvent) => {
    e.preventDefault();
    const enteredText = todoTextInput.current!.value;

    if (enteredText.trim().length === 0) {
      //throw an error
      return;
    }

    todosCtx.addTodo(enteredText);
  };
  return (
    <div className="form">
      <form onSubmit={submitHandler}>
        <label htmlFor="text">Todo Text</label>
        <input ref={todoTextInput} type="text" id="text" />
        <button>Add Todo</button>
      </form>
    </div>
  );
};

export default NewTodo;
